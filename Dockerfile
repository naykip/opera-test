FROM golang:1.14 AS builder


WORKDIR /


ADD . service/

WORKDIR /service/
RUN go build -ldflags="-w -s" main.go


#FROM scratch

#COPY --from=builder /service/main /service/main
#COPY --from=builder /service/scratch.html  /service/scratch.html


EXPOSE 8080

ENTRYPOINT ["/service/main"]

