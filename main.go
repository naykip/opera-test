package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

const frontendPath = "./scratch.html"

func main() {
	http.HandleFunc("/", hello)

	fmt.Printf("Starting server for testing HTTP POST...\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func hello(w http.ResponseWriter, r *http.Request) {
	if r.Body != nil {
		defer r.Body.Close()
	}

	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET" )
	w.Header().Set("Access-Control-Allow-Headers","Content-Type,access-control-allow-origin, access-control-allow-headers")

	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	
	switch r.Method {
	case "GET":
		http.ServeFile(w, r, frontendPath)
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "bad request", http.StatusBadRequest)
			return
		}

		fmt.Println(string(body))

		if _, err := fmt.Fprintf(w, "post: %s\n", string(body)); err != nil {
			log.Println(err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		if _, err := fmt.Fprintf(w, "User Agent: %s\n", r.UserAgent()); err != nil {
			log.Println(err.Error())
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	default:
		http.Error(w, "Sorry, only GET and POST methods are supported", http.StatusMethodNotAllowed)
		return
	}
}

